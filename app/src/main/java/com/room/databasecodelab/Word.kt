package com.room.databasecodelab

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by NagarjunaReddy
 */

@Entity(tableName = "word_table")
data class Word(
    @PrimaryKey
    @NonNull
    val id: String,
    @ColumnInfo(name = "Serial_number") val serialNo: String,
    @ColumnInfo(name = "word") val word: String
)