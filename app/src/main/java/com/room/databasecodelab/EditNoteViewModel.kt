package com.room.databasecodelab

import android.app.Application
import android.util.Log
import androidx.annotation.NonNull
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope

/**
 * Created by NagarjunaReddy
 */

class EditNoteViewModel(@NonNull application: Application) : AndroidViewModel(application) {

    private val TAG = this.javaClass.simpleName
    private val wordDao: WordDao
    private val db: WordRoomDatabase

    init {
        Log.i(TAG, "Edit ViewModel")
        db = WordRoomDatabase.getDatabase(application, viewModelScope)
        wordDao = db.wordDao()
    }

    fun getWord(noteId: String): LiveData<Word> {
        return wordDao.getWord(noteId)
    }

    fun getSerialNo(serialNo: String): LiveData<Word> {
        return wordDao.getSerialNo(serialNo)
    }

    override fun onCleared() {
        super.onCleared()
        Log.i(TAG, "ViewModel Destroyed")
    }
}
