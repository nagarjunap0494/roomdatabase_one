package com.room.databasecodelab

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by NagarjunaReddy
 */

class WordViewModel(application: Application) : AndroidViewModel(application) {
    val TAG: String = this.javaClass.simpleName

    // The ViewModel maintains a reference to the repository to get data.
    private val repository: WordRepository
    // LiveData gives us updated words when they change.
    val allWords: LiveData<List<Word>>

    init {
        // Gets reference to WordDao from WordRoomDatabase to construct
        // the correct WordRepository.
        var wordDao = WordRoomDatabase.getDatabase(
            application,
            viewModelScope
        ).wordDao()
        repository = WordRepository(wordDao)
        allWords = repository.allWords
    }

    /**
     * The implementation of insert() in the database is completely hidden from the UI.
     * Room ensures that you're not doing any long running operations on
     * the main thread, blocking the UI, so we don't need to handle changing Dispatchers.
     * ViewModels have a coroutine scope based on their lifecycle called
     * viewModelScope which we can use here.
     */
    /**
     * Name of the property that defines the maximal number of threads that are used by [Dispatchers.IO] coroutines dispatcher.
     */
    fun insert(word: Word) = viewModelScope.launch(Dispatchers.Default) {
        repository.insert(word)
    }

    /**
     * Name of the property that defines the maximal number of threads that are used by [Dispatchers.IO] coroutines dispatcher.
     */
    fun update(word: Word) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(word)
    }

    /**
     * Name of the property that defines the maximal number of threads that are used by [Dispatchers.IO] coroutines dispatcher.
     */
    fun delete(word: Word) = viewModelScope.launch(Dispatchers.IO){
        repository.delete(word)
    }

    override fun onCleared() {
        super.onCleared()
        Log.i(TAG, "ViewModel Destroyed")
    }
}