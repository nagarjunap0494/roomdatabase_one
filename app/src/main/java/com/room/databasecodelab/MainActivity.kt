package com.room.databasecodelab

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

/**
 * Created by NagarjunaReddy
 */
class MainActivity : AppCompatActivity(), WordListAdapter.OnDeleteClickListener {

    val TAG: String = this.javaClass.simpleName
    private lateinit var wordViewModel: WordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = WordListAdapter(this, this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?

        //wordViewModel = ViewModelProviders.of(this).get(WordViewModel::class.java)
        wordViewModel =
            ViewModelProvider(this, ViewModelProviderFactory(WordViewModel(application))).get(
                WordViewModel::class.java
            )

        wordViewModel.allWords.observe(this, Observer { words ->
            // Update the cached copy of the words in the adapter.
            words?.let { adapter.setWords(it) }
        })

        fab.setOnClickListener {
            val intent = Intent(this@MainActivity, NewWordActivity::class.java)
            startActivityForResult(intent, NEW_WORD_ACTIVITY_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == NEW_WORD_ACTIVITY_REQUEST && resultCode == Activity.RESULT_OK) {
            val note_id = UUID.randomUUID().toString()
            data?.let {
                val word = Word(
                    note_id,
                    it.getStringExtra(NewWordActivity.SERIAL_NUMBER),
                    it.getStringExtra(NewWordActivity.NEW_WORD)
                )
                wordViewModel.insert(word)

                Toast.makeText(
                    applicationContext,
                    "data Inserted",
                    Toast.LENGTH_LONG
                ).show()
            }
        } else if (requestCode == EDIT_WORD_ACTIVITY_REQUEST && resultCode == Activity.RESULT_OK) {
            data?.let {

                val word = Word(
                    it.getStringExtra(EditActivity.WORD_ID),
                    it.getStringExtra(EditActivity.SERIAL_NO),
                    it.getStringExtra(EditActivity.WORD_TEXT)
                )
                Log.i(TAG, word.word)
                wordViewModel.update(word)

                Toast.makeText(
                    applicationContext,
                    "data updated",
                    Toast.LENGTH_LONG
                ).show()
            }
        } else {
            Toast.makeText(
                applicationContext,
                R.string.empty_not_saved,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun OnDeleteClickListener(myNote: Word) {
        // Code for Delete operation
        wordViewModel.delete(myNote)
    }

    companion object {

        const val NEW_WORD_ACTIVITY_REQUEST = 1
        const val EDIT_WORD_ACTIVITY_REQUEST = 2
    }
}