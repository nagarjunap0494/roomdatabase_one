package com.room.databasecodelab

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_edit.*

/**
 * Created by NagarjunaReddy
 */

class EditActivity : AppCompatActivity() {
    var mBundle: Bundle? = null
    var mWordId: String? = null
    var mSerialNo: String? = null
    var mWord: LiveData<Word>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        mBundle = intent.extras

        if (mBundle != null) {
            mWordId = mBundle!!.getString("WORD_ID")
            mSerialNo = mBundle!!.getString("SERIAL_NO")
        }

        //noteModel = ViewModelProvider.of(this).get(EditNoteViewModel::class.java)
        val noteModel =
            ViewModelProvider(this, ViewModelProviderFactory(EditNoteViewModel(application))).get(
                EditNoteViewModel::class.java
            )

        mWord = mWordId?.let { noteModel.getWord(it) }
        mWord!!.observe(this, Observer { word ->
            etNote!!.setText(word!!.word)
        })

        mWord = mWordId?.let { noteModel.getSerialNo(it) }
        mWord!!.observe(this, Observer { serialNo ->
            etNumber!!.setText(serialNo!!.serialNo)
        })
    }

    fun updateNote(view: View) {
        val updatedNote = etNote!!.text.toString()
        val updateNumber = etNumber!!.text.toString()
        val resultIntent = Intent()
        resultIntent.putExtra(WORD_ID, mWordId)
        resultIntent.putExtra(SERIAL_NO, updateNumber)
        resultIntent.putExtra(WORD_TEXT, updatedNote)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    fun cancelUpdate(view: View) {
        finish()
    }

    companion object {
        const val WORD_ID = "WORD_ID"
        const val WORD_TEXT = "WORD_TEXT"
        const val SERIAL_NO = "SERIAL_NO"
    }
}