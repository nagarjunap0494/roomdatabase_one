package com.room.databasecodelab

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_new_word.*

/**
 * Created by NagarjunaReddy
 */
class NewWordActivity : AppCompatActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_word)

        val button = findViewById<Button>(R.id.button_save)
        button.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(edit_word.text) && TextUtils.isEmpty(edit_word_number.text)) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                val word = edit_word.text.toString()
                val number = edit_word_number.text.toString()
                //var bundle = Bundle()
                replyIntent.putExtra(NEW_WORD, word)
                replyIntent.putExtra(SERIAL_NUMBER, number)
                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }

    companion object {
        const val NEW_WORD = "RoomDatabase.WORD"
        const val SERIAL_NUMBER = "RoomDatabase.NUMBER"
    }
}