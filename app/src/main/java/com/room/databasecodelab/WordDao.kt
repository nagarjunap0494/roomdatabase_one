package com.room.databasecodelab

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * Created by NagarjunaReddy
 */

@Dao
interface WordDao {

    @Query("SELECT * from word_table ORDER BY word ASC")
    fun getAlphabetizedWords(): LiveData<List<Word>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(word: Word)

    @Query("DELETE FROM word_table")
    suspend fun deleteAll()

    @Query("SELECT * from word_table WHERE id=:wordId")
    fun getWord(wordId: String): LiveData<Word>

    @Query("SELECT * from word_table WHERE id=:serialNo")
    fun getSerialNo(serialNo: String): LiveData<Word>

    @Update
    fun update(word: Word)

    @Delete
    fun delete(word: Word): Int

}