package com.room.databasecodelab

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by NagarjunaReddy
 */

class WordListAdapter internal constructor(
    private val mContext: Context,
    private val onDeleteClickListener: OnDeleteClickListener?
) :
    RecyclerView.Adapter<WordListAdapter.WordViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(mContext)
    private var mWords = emptyList<Word>() // Cached copy of words

    interface OnDeleteClickListener {
        fun OnDeleteClickListener(myNote: Word)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return WordViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = mWords[position]
        holder.setData(current.word, current.serialNo, position)
        holder.setListeners();
    }

    internal fun setWords(words: List<Word>) {
        this.mWords = words
        notifyDataSetChanged()
    }

    override fun getItemCount() = mWords.size

    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val noItemView: TextView = itemView.findViewById(R.id.textView_no)
        val wordItemView: TextView = itemView.findViewById(R.id.textView_title)
        val editImageview: ImageView = itemView.findViewById(R.id.imageView_edit)
        val imageViewDelete: ImageView = itemView.findViewById(R.id.imageView_delete)
        private var mPosition: Int = 0

        fun setData(word: String, serialNo: String, position: Int) {
            noItemView.text = serialNo
            wordItemView.text = word
            mPosition = position
        }

        fun setListeners() {
            editImageview.setOnClickListener {
                val intent = Intent(mContext, EditActivity::class.java)
                intent.putExtra("WORD_ID", mWords[mPosition].id)
                intent.putExtra("SERIAL_NO", mWords[mPosition].serialNo)
                (mContext as Activity).startActivityForResult(
                    intent,
                    MainActivity.EDIT_WORD_ACTIVITY_REQUEST
                )
            }

            imageViewDelete.setOnClickListener {
                onDeleteClickListener?.OnDeleteClickListener(mWords[mPosition])
            }
        }
    }
}